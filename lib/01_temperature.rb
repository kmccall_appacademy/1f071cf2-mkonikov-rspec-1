def ftoc(n)
  (n.to_f - 32) * 0.55555555555555555
end

def ctof(n)
  n * 1.8 + 32
end

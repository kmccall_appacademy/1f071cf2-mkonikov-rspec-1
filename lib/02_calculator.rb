def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(ary)
  ary.empty? ? 0 : ary.reduce(:+)
end

def multiply(ary)
  ary.reduce(:*)
end

def power(a,b)
  a ** b
end

def factorial(n)
  return 1 if n == 0
  (1..n).reduce { |a,el| a * el }
end

def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, n=2)
  result = Array.new(n) {string}.join(" ")
end

def start_of_word(string, n)
  string[0...n]
end

def first_word(string)
  string.split(" ").first
end

def titleize(string)
  small = ['and', 'the', 'over']
  words = string.split(" ")
  words.each { |x| x.capitalize! unless small.include?(x) && x != words[0] }
  words.join(" ")
end

def translate_word(string)
  vowels = ['a','e','i','o','u']
  if vowels.include?(string[0])
    string << 'ay'
  else
    i = 1
    i += 1 until vowels.include?(string[i]) && string[i-1] != 'q'
    string = string.slice(i, string.length) + string.slice(0,i) + 'ay'
  end
end

def translate(string)
  string.split(" ").map { |word| translate_word(word) }.join(" ")
end
